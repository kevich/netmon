<?php

include '../../classes/MySQL.class.php';
include '../../classes/Common.class.php';

$class=Common::get_class_name($_REQUEST['table']);

if (file_exists('../../classes/'.$class.'.class.php')) {

	include '../../classes/'.$class.'.class.php';

	$GLOBALS['NETMON_SQL']=new MySQL;

	$GLOBALS['NETMON_SQL']->connect();

	$item=new $class;

	$item->InsertNewItem();

	$arr=array('status'=>true,'answer'=>'Запись добавлена','item'=>$item->draw_item());
	echo json_encode($arr);

	$GLOBALS['NETMON_SQL']->close();

}
else {
	$GLOBALS['NETMON_SQL']=new MySQL;

	$GLOBALS['NETMON_SQL']->connect();

	$table=$_REQUEST['table'];
	switch ($table) {
	    case "ifaces":
			$ip="192.168.0.0";
			$pair=$_REQUEST['pair'];
	        $GLOBALS['NETMON_SQL']->exec_query("INSERT INTO $table SET ip='$ip',servers_id=$pair");
	        $arr=array('status'=>true,'answer'=>'Запись добавлена','item'=>"<div id=\"ifaces-".$GLOBALS['NETMON_SQL']->getCreatedId()."\"><span class=\"editable\" id=\"ip-".$GLOBALS['NETMON_SQL']->getCreatedId()."\" table=\"ifaces\">$ip</span><img class=\"small_delete cursor\" src=\"img/small_delete.png\" onclick=\"delete_item(".$GLOBALS['NETMON_SQL']->getCreatedId().",'ifaces');\" title=\"Удалить интерфейс\" /></div>");
	        echo json_encode($arr);
	        break;
	    case "servers2services":
			//$select=Common::render_select('services',0,'color','servers2services;services_id;servers_id:'.);
			$query="SELECT id,name,color FROM services WHERE id NOT IN (SELECT  services_id FROM servers2services WHERE servers_id=".$_REQUEST['pair'].") LIMIT 1";
			$res=$GLOBALS['NETMON_SQL']->exec_query($query);
			if ($res->num_rows==1) {
				$row=mysqli_fetch_assoc($res);
				$GLOBALS['NETMON_SQL']->exec_query("INSERT INTO servers2services SET services_id='".$row['id']."',servers_id=".$_REQUEST['pair']);
	        $select="<div class=\"service\" style=\"background-color:#".$row['color']."\" id=\"servers2services{$row['id']}-{$_REQUEST['pair']}\"><span id=\"services_id-{$row['id']}-{$_REQUEST['pair']}\" class=\"editable select m2m\" m2m=\"servers2services\" pair=\"servers_id-{$_REQUEST['pair']}\" table=\"services\">".$row['name']."</span><img class=\"small_delete cursor\" src=\"img/small_delete.png\" onclick=\"delete_item({$row['id']},'servers2services','services_id;servers_id:{$_REQUEST['pair']}');\"  title=\"Удалить сервис\" /></div>";
	        $ans='Запись добавлена';
			}
			else {
				$ans='<span style="color:red;">Нет доступных сервисов</span>';
				$select="";
			}

	        $arr=array('status'=>true,'answer'=>$ans,'item'=>$select);
	        
	        echo json_encode($arr);
	        break;
	    default:
			$values=json_decode(urldecode($_REQUEST['values']));
			$set=array();

			foreach($values as $name=>$val) {
				$set[]="$name='".$GLOBALS['NETMON_SQL']->real_escape_string($val)."'";
			}
			//echo "INSERT INTO ".self::table." SET ".Common::generate_insert($this);
			$GLOBALS['NETMON_SQL']->exec_query("INSERT INTO $table SET ".implode(',',$set));
			$arr=array('status'=>true,'answer'=>'Запись добавлена');
			echo json_encode($arr);
	        break;
	    
	}
	
}
