<?php
class Network {
	const table='td_filialsName';
    public $id;
    public $name;
    public $nfsserv;
    public $sub;
    public $posx;
    public $posy;

    public function draw_item() {
		$content="";
		//$content.="<div class=\"window rest\" id=\"td_filialsName-".$this->id."\" style=\"top:".$this->posy."px;left:".$this->posx."px;\">";
		$content.="<div class=\"window\" id=\"td_filialsName-".$this->id."\">";
		$content.="<div class=\"name\"><a href=\"?view=filial&id=".$this->sub."\">".$this->name."</a></div>";
		$content.="<div><span class=\"name\">Подсеть:&nbsp;</span>".$this->sub."</div>";
		$content.="<div><span class=\"name\">NFS-server:&nbsp;</span><br />".$this->nfsserv."</div>";
		$sql="SELECT
			COUNT(mac) AS total,
			SUM((TIMESTAMPDIFF(DAY,refresh,CURDATE())<=30)*(activity=1)) AS active,
			SUM((TIMESTAMPDIFF(DAY,refresh,CURDATE())<=30)*(activity=0)) AS off,
			SUM((TIMESTAMPDIFF(DAY,refresh,CURDATE())>30)*(activity=1)) AS strange,
			SUM((TIMESTAMPDIFF(DAY,refresh,CURDATE())>30)*(activity=0)) AS old
			FROM computers
			WHERE subnetwork=".$this->sub;
		$res=$GLOBALS['NETMON_SQL']->exec_query($sql);
		$it=mysqli_fetch_assoc( $res );
		$content.="<div><span class=\"info_active\"><a>{$it['active']}</a></span>+
						<span class=\"info_normal\"><a>{$it['off']}</a></span>+
						<span class=\"info_strange\"><a>{$it['strange']}</a></span>+
						<span class=\"info_old\"><a>{$it['old']}</a></span>=
						<span class=\"info_total\"><b><a>{$it['total']}</a></b></span></div>";
		$content.="<div><span class=\"control\">{$GLOBALS['RENDER_DEF_CONTROLS']}</span></div>";
		$content.="</div>\n";
		return $content;
    }

    static function GetJsModule() {
		echo "<script type='text/javascript' src='js/liveSearch.js'></script><script type='text/javascript'>window.table_glob='".self::table."';
		$('#menu_point').before('<li><input id=\"search_by_name\" type=\"text\" placeholder=\"Поиск пользователя\" /></li>');$('#search_by_name').liveSearch({url: 'ajax/ajax.searchUser.php' + '?name='});</script>";
	}

    static function getItems($where="") {
		$wrapped=array();
		$props=Common::generate_list("Network");
		$res=$GLOBALS['NETMON_SQL']->exec_SELECTquery(implode(',',$props),'td_filialsName',$where,false,"name ASC");
		while ($row=mysqli_fetch_assoc($res)) {
			$item=new Network;
			foreach($props as $val) {
				$item->$val=$row[$val];
			}
			array_push($wrapped,$item);
		}
		return $wrapped;
    }
}