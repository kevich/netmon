<?php

class Computer {
	const table='computers';
	
    public $subnetwork;
    public $id;
    public $ip;
    public $mac;
    public $os_ver;
    public $user;
    public $refresh;
    public $activity;
    public $phone;
    public $posx;
    public $posy;

        public function draw_item() {
		$content="";
		$days=(int)((time()-strtotime($this->refresh))/86400);
		$class=(($this->activity=='1')?'alive':'normal');
		if ($days>30){
			if ($class!='normal') $class='strange';
			else $class='old';

		}
		$content.="<div class=\"draggable ".$class."\" id=\"computers-".$this->id."\" style=\"top:".$this->posy."px;left:".$this->posx."px;\">";
		$content.="<div class=\"name\"><img src=\"img/comp.png\" />".$this->ip."</div>";
		$content.="<div><span class=\"name\">MAC:&nbsp;</span>".$this->mac."</div>";
		$content.="<div><span class=\"name\">Пользователь:&nbsp;</span><br />".$this->user."</div>";
		$content.="<div><span class=\"name\">Вошли &nbsp;</span>".$days." дней назад</div>";
		$content.="<div><span class=\"name\">OS:&nbsp;</span>".$this->os_ver."</div>";
		$content.="<div><span class=\"name\">Телефон:&nbsp;</span>".$this->phone."</div>";
		$res=$GLOBALS['NETMON_SQL']->exec_SELECTquery('mac','movement','mac=\''.$this->mac.'\'');
		if (mysqli_num_rows($res)>=1) $movement="<img class=\"movements cursor\" src=\"img/movements.png\" move_id=\"{$this->mac}\" title=\"Посмотреть перемещения\" />";
		else $movement="";
		$content.="<div><span class=\"control\">".Common::put_controls(array('delete'=>$this->id))."$movement</div>";
		$content.="</div>\n";
		return $content;
    }

    static function GetJsModule() {
		echo "<script type='text/javascript' src='js/liveSearch.js'></script><script type='text/javascript'>window.table_glob='".self::table."';
		$('#menu_point').before('<li><input id=\"search_by_name\" type=\"text\" placeholder=\"Поиск пользователя\" /></li>');$('#search_by_name').liveSearch({url: 'ajax/ajax.searchUser.php' + '?name='});</script>";
	}

    static function getItems($where="") {
		$wrapped=array();
		$props=Common::generate_list("Computer");
		$res=$GLOBALS['NETMON_SQL']->exec_SELECTquery(implode(',',$props),'computers',$where);
		while ($row=mysqli_fetch_assoc($res)) {
			$item=new Computer;
			foreach($props as $val) {
				$item->$val=$row[$val];
			}
			array_push($wrapped,$item);
		}
		return $wrapped;
    }
}