<?php

include_once 'Service.class.php';

class Server {
	const table='servers';

    public $id;
    public $name;
    public $ifaces=array();
    public $services=array();
    public $posx;
    public $posy;
    

	function InsertNewItem($values=false) {
		$this->name="НовыйСервер";
		$this->posx="20";
		$this->posy="30";
		$this->ifaces="";
		$this->services="";
		if ($values) {
			foreach($values as $name=>$val) {
			    $this->$name=$val;
			}
		}
		//echo "INSERT INTO ".self::table." SET ".Common::generate_insert($this);
		$GLOBALS['NETMON_SQL']->exec_query("INSERT INTO ".self::table." SET ".Common::generate_insert($this));
		$this->id=$GLOBALS['NETMON_SQL']->getCreatedId();
		return $GLOBALS['NETMON_SQL']->getCreatedId();

	}
    
    public function draw_item() {
		$content="";
		$content.="<div class=\"draggable\" id=\"servers-".$this->id."\" style=\"top:".$this->posy."px;left:".$this->posx."px;\">";
		$content.="<div><span class=\"name editable\" table=\"servers\" id=\"name-{$this->id}\">".$this->name."</span></div>";
		$content.="<div><span class=\"name\">ifaces:</span><img class=\"small_add cursor\" onclick=\"add_item('ifaces',{$this->id});\" src=\"img/small_add.png\" title=\"Добавить интерфейс\" /></div><div id=\"ifaces_{$this->id}\">";
		foreach($this->ifaces as $iface) {
			$content.="<div id=\"ifaces-{$iface->id}\"><span class=\"editable\" id=\"ip-{$iface->id}\" table=\"ifaces\">{$iface->ip}</span><img class=\"small_delete cursor\" src=\"img/small_delete.png\" onclick=\"delete_item({$iface->id},'ifaces');\" title=\"Удалить интерфейс\" /></div>";
		}
		$content.="</div>";
		$content.="<div id=\"servers2services_{$this->id}\"><b>Сервисы:<img class=\"small_add cursor\" src=\"img/small_add.png\" onclick=\"add_item('servers2services',{$this->id});\" title=\"Добавить сервис\" /></b>";
		foreach($this->services as $service) {
			$content.="<div class=\"service connectable\" style=\"background-color:#".$service->color."\" id=\"servers2services{$service->id}-{$this->id}\"><span id=\"services_id-{$service->id}-{$this->id}\" class=\"editable select m2m\" m2m=\"servers2services\" pair=\"servers_id-{$this->id}\" table=\"services\">".$service->name."</span><img class=\"small_delete cursor\" src=\"img/small_delete.png\" onclick=\"delete_item({$service->id},'servers2services','services_id;servers_id:{$this->id}');\" title=\"Удалить сервис\" />
			<div class=\"sourcePoint\" id=\"source-servers2services-{$service->id}-{$this->id}\" name=\"{$service->name}\" color=\"{$service->color}\"></div>
			</div>";
		}
		//$content.="<div></div>";
		$content.="</div>\n";
		$content.="<div><span class=\"control\">".Common::put_controls(array('delete'=>$this->id,'info'=>$this->name.'--'.$this->id))."</span></div>";
		$content.="</div>\n";
		return $content;
    }

    static function getItems($where="") {
		$wrapped=array();
		$res=$GLOBALS['NETMON_SQL']->exec_SELECTquery('id,name,posx,posy','servers',$where);
		while ($row=mysqli_fetch_assoc($res)) {
			$item=new Server;
			$item->id=$row['id'];
			$item->name=$row['name'];
			$item->posx=$row['posx'];
			$item->posy=$row['posy'];
			$ifaces_res=$GLOBALS['NETMON_SQL']->exec_SELECTquery('ip,id','ifaces','servers_id='.$row['id']);
			while ($it=mysqli_fetch_assoc( $ifaces_res )) {
				$ob=new stdClass;
				$ob->id=$it['id'];
				$ob->ip=$it['ip'];
				array_push($item->ifaces,$ob);
			}
			$serv_props=Common::generate_list('Service');
			$service_res=$GLOBALS['NETMON_SQL']->exec_query("SELECT s.* FROM ".Service::getTable()." s, servers2services s2s WHERE s2s.services_id=s.id AND s2s.servers_id=".$item->id);
			while ($serv=mysqli_fetch_assoc( $service_res )) {
				$service=new Service;
				foreach($serv_props as $val) {
					$service->$val=$serv[$val];
				}
				array_push($item->services,$service);
			}
			array_push($wrapped,$item);
		}
		return $wrapped;
    }

    static function GetJsModule() {
		echo "<script type='text/javascript'>window.table_glob='".self::table."';
		$('#menu_point').before('<li><a href=\"#\" onclick=\"add_item();return false;\"><img src=\"img/add.png\" /> Добавить сервер</a></li>');</script>";
	}
}
// subnetwork 	id 	ip 	mac 	trojan_version 	os_ver 	firmware_ver 	size 	user 	groupSquid Группа в сквиде, для trojanSIT	groupIpTables группа в iptables для trojanSIT	refresh 	activity Активность компьютера	phone


?>