<?php

class Common {

    static function generate_list($type) {
		$out=array();
		$vars=get_class_vars($type);
		foreach($vars as $name=>$val) {
			if(gettype($val)!="array") array_push($out,$name);
		}
		return $out;
	}

	static function generate_insert($object) {
		$return = array();
		$vars=get_class_vars(get_class($object));
		foreach($vars as $name=>$bulk) {
			if ($object->$name!="") $return[]= "$name='".$object->$name."'";
		}
		return implode(', ', $return);
	}

	static function get_class_name($table) {
		$classes=array('services'=>'Service','servers'=>'Server');
		if (array_key_exists($table,$classes)) return $classes[$table];
		else return false;
	}

	static function put_controls($items) {
		$item_types=array(
			'delete' => '<img class="delete cursor" onclick="delete_item([+opt+]);" title="Удалить" src="img/delete.png" />',
			'edit' => '<img src=\"img/edit.png\" class=\"edit cursor\" />',
			'info' => '<img src="img/info.png" class="info cursor" move_id="[+opt+]" title="Информация" />'
		);
		$content="";
		foreach($items as $val=>$opt) {
		    $content.=str_replace('[+opt+]',$opt,$item_types[$val]);
		}
		return $content;
		
	}

	static function render_select($table_main="",$id=0,$addit="",$parent_id="") {
		include_once 'MySQL.class.php';
		if (!array_key_exists('NETMON_SQL',$GLOBALS)){
			$GLOBALS['NETMON_SQL']=new MySQL;

			$GLOBALS['NETMON_SQL']->connect();
		}

		$out="<select id=\"inline_edit\">";

		if ($parent_id!="") {
			$parent_id=explode(';',$parent_id);
			$table=$parent_id[0];
			$what=$parent_id[1];
			$where=str_replace(':','=',$parent_id[2]);
			$query="SELECT id,name".($addit!=""?",".$addit:"")." FROM $table_main WHERE id NOT IN (SELECT  $what FROM $table WHERE $where)";
		}
		else $query="SELECT id,name".($addit!=""?",".$addit:"")." FROM $table_main";

		$res=$GLOBALS['NETMON_SQL']->exec_query($query);
			while ($row=mysqli_fetch_assoc($res)) {
				$opt="";
				foreach(explode(',',$addit) as $val) {
					$opt.=" $val=\"{$row[$val]}\"";
				}

				$out.="<option ".($id==$row['id']?"selected ":"")." value=\"{$row['id']}\" $opt>{$row['name']}</option>";
			}

		$out.="</select>";
		return $out;
	}
}
