<?php

include_once 'Common.class.php';


class Service {
	const table='services';
	public $name;
	public $id;
	public $color;

	function InsertNewItem($values=false) {
		$this->name="НовыйСервис";
		$this->color="ffffff";
		if ($values) {
			foreach($values as $name=>$val) {
			    $this->$name=$val;
			}
		}
		$GLOBALS['NETMON_SQL']->exec_query("INSERT INTO ".self::table." SET ".Common::generate_insert($this));
		$this->id=$GLOBALS['NETMON_SQL']->getCreatedId();
		return $GLOBALS['NETMON_SQL']->getCreatedId();
		
	}
	
	static function getTable() {
        return self::table;
    }

	static function getItems($where="") {
		$wrapped=array();
		$props=Common::generate_list(get_class($this));
		$res=$GLOBALS['NETMON_SQL']->exec_SELECTquery(implode(',',$props),self::table,$where);
		while ($row=mysqli_fetch_assoc($res)) {
			$item=new Service;
			foreach($props as $val) {
				$item->$val=$row[$val];
			}
			array_push($wrapped,$item);
		}
		return $wrapped;
		
	}

	static function GetJsModule() {
		echo "<script type='text/javascript'>window.table_glob='".self::table."';
		$('#menu_point').before('<li><a href=\"#\" onclick=\"add_item();return false;\"><img src=\"img/add.png\" /> Добавить сервис</a></li>');</script>";
	}

	static function GetBottomContent() {
		echo "<span id=\"insert_point\"></span>";
	}
    
	public function draw_item() {
		$content="";
		$content.="<div class=\"small_items ".self::table."\" id=\"".self::table."-".$this->id."\" style=\"background-color:{$this->color};\">";
		$content.="<div><span class=\"name editable\" table=\"".self::table."\" id=\"name-{$this->id}\">".$this->name."</span></div>";
		$content.="<div><b>Цвет:&nbsp;</b><span class=\"color editable\" table=\"".self::table."\" id=\"color-{$this->id}\">".$this->color."</span></div>";
		$content.="<div><span class=\"control\">".Common::put_controls(array('delete'=>$this->id,'info'=>$this->name.'--'.$this->id))."</div>";
		$content.="</div>\n";
		return $content;
    }
}
?>