/*
;(function() {
	window.jsPlumbObject = new Object();
	window.jsPlumbObject.connectorHoverStyle = {
			lineWidth:5,
			strokeStyle:"#2e2aF8"
		};
	window.jsPlumbObject.connectorPaintStyle = {
			lineWidth:3,
			strokeStyle:"#deea18",
			joinstyle:"round"
		};
	window.jsPlumbObject.targetEndpoint = {
			endpoint:"Dot",
			paintStyle:{ fillStyle:"#558822",radius:4 },
			hoverPaintStyle:window.jsPlumbObject.connectorHoverStyle,
			maxConnections:-1,
			dropOptions:{ hoverClass:"hover", activeClass:"active" },
			isTarget:true,
			overlays:[
				[ "Label", { location:[0.5, -0.5],
					//label:"Drop",
					cssClass:"endpointTargetLabel" } ]
			]
		};
		// a source endpoint that sits at BottomCenter
		window.jsPlumbObject.bottomSource = jsPlumb.extend( { anchor:"BottomCenter" }, window.jsPlumbObject.sourceEndpoint);
		// the definition of source endpoints (the small blue ones)
		window.jsPlumbObject.sourceEndpoint = {
			endpoint:"Dot",
			paintStyle:{ fillStyle:"#225588",radius:4 },
			isSource:true,
			maxConnections:-1,
			connector:[ "Flowchart", { stub:40 } ],
			connectorStyle:window.jsPlumbObject.connectorPaintStyle,
			hoverPaintStyle:window.jsPlumbObject.connectorHoverStyle,
			connectorHoverStyle:window.jsPlumbObject.connectorHoverStyle,
			dragOptions:{},
			overlays:[
				[ "Label", {
					location:[0.5, 1.5],
					//label:"",
					cssClass:"endpointSourceLabel"
				} ]
			]
		};
		window.jsPlumbObject.init = function() {
				
			jsPlumb.importDefaults({
				// default drag options
				DragOptions : { cursor: 'pointer', zIndex:2000 },
				// default to blue at one end and green at the other
				EndpointStyles : [{ fillStyle:'#225588' }, { fillStyle:'#558822' }],
				// blue endpoints 7 px; green endpoints 11.
				Endpoints : [ [ "Dot", {radius:4} ], [ "Dot", { radius:4 } ]],
				// the overlays to decorate each connection with.  note that the label overlay uses a function to generate the label text; in this
				// case it returns the 'labelText' member that we set on each connection in the 'init' method below.
				ConnectionOverlays : [
					[ "Arrow", { location:0.9 } ],
					[ "Label", { 
						location:0.1,
						id:"label",
						cssClass:"aLabel"
					}]
				]
			});			

			// this is the paint style for the connecting lines..
			var init = function(connection) {
				connection.getOverlay("label").setLabel(connection.sourceId.substring(6) + "-" + connection.targetId.substring(6));
			};			
					
			// listen for new connections; initialise them the same way we initialise the connections at startup.
			jsPlumb.bind("jsPlumbConnection", function(connInfo, originalEvent) { 
				init(connInfo.connection);
			});
						
			
			// connect a few up
			//jsPlumb.connect({uuids:["window2BottomCenter", "window3TopCenter"]});
			//jsPlumb.connect({uuids:["window2LeftMiddle", "window4LeftMiddle"]});
			//jsPlumb.connect({uuids:["window4TopCenter", "window4RightMiddle"]});
			//jsPlumb.connect({uuids:["window3RightMiddle", "window2RightMiddle"]});
			//jsPlumb.connect({uuids:["window4BottomCenter", "window1TopCenter"]});
			//jsPlumb.connect({uuids:["window3BottomCenter", "window1BottomCenter"]});

			//
			// listen for clicks on connections, and offer to delete connections on click.
			//
			jsPlumb.bind("click", function(conn, originalEvent) {
				if (confirm("Delete connection from " + conn.sourceId + " to " + conn.targetId + "?"))
					jsPlumb.detach(conn); 
			});
			
		};
		window.jsPlumbObject._addEndpoints = function(toId, sourceAnchors, targetAnchors) {
			var allSourceEndpoints = [], allTargetEndpoints = [];
			for (var i = 0; i < sourceAnchors.length; i++) {
				var sourceUUID = toId + sourceAnchors[i];
				allSourceEndpoints.push(jsPlumb.addEndpoint(toId, window.jsPlumbObject.sourceEndpoint, { anchor:sourceAnchors[i], uuid:sourceUUID }));
			}
			for (var j = 0; j < targetAnchors.length; j++) {
				var targetUUID = toId + targetAnchors[j];
				allTargetEndpoints.push(jsPlumb.addEndpoint(toId, window.jsPlumbObject.targetEndpoint, { anchor:targetAnchors[j], uuid:targetUUID }));
			}
		};
	//};
})();

*/
/*

jsPlumb.bind("ready", function() {
// chrome fix.
document.onselectstart = function () { return false; };
window.jsPlumbObject.init();

// explanation div is draggable
$("#explanation,.renderMode").draggable();

});*/
jsPlumb.setRenderMode(jsPlumb.SVG);

initEndpoints = function() {
	$(".sourcePoint").each(function(i,e) {
		var p = $(e).parent();
		jsPlumb.makeSource($(e), {
			anchor:"RightMiddle",
			connector:[ "Bezier"],
			connectorStyle:{ strokeStyle:'#'+$(this).attr('color'), lineWidth:2, outlineColor:"#666" },
			connectorHoverPaintStyle: {strokeStyle:"#2e2aF8"},
			maxConnections:-1,
			isSource: true
		});
	});
};

jsPlumb.ready(function() {
	jsPlumb.importDefaults({
		Container:$("body"),
		Endpoint : ["Dot", {radius:2}],
		HoverPaintStyle : {strokeStyle:"red", lineWidth:4 },
		ConnectionOverlays : [
			[ "Arrow", {
				location:.5,
				id:"arrow",
				foldback:0.8
			} ],
			[ "Label", { label:"", id:"label" }]
		],
		MaxConnections:-1
	});

	make_draggable();

	initEndpoints();

	window.rendering=false;

	jsPlumb.bind("jsPlumbConnection", function(conn) {
		if (!window.rendering) {
			var values=new Object();
			values["source_id"]=$(conn.source).attr('id');
			values["target_id"]=$(conn.target).attr('id');
			values["view"]=window.page;
			var table="connections";
			$.ajax({
				type: "POST",
				url: "ajax/bd/ajax.add.php",
				dataType: "json",
				data: "table="+table+"&values="+encodeURIComponent(JSON.stringify(values)),
				success: function(msg) {
					if (msg.status) {
						conn.connection.setPaintStyle({strokeStyle:'#'+$(conn.source).attr('color'), lineWidth:2, outlineColor:"#666"});
					}
				}
			}).fail(function() {$('#alerts').html("<span style='color:red;'>Не удалось добавить объект</span>").trigger('show-alert'); });
		}
		else conn.connection.setPaintStyle({strokeStyle:'#'+$(conn.source).attr('color'), lineWidth:2,outlineColor:"#666"});
		//conn.connection.setHoverStyle({strokeStyle:"red"});

	});

	jsPlumb.makeTarget($(".connectable"), {
		dropOptions:{ hoverClass:"dragHover" },
		anchor:"LeftMiddle",
		isTarget: true

	});

	jsPlumb.bind("click", function(conn, originalEvent) {
		if (confirm("Удалить соединение от '" + $(conn.source).attr('name') + "' к '" + $(conn.target).text().trim() + "'?"))
			$.ajax({
				type: "POST",
				url: "ajax/bd/ajax.delete.php",
				dataType: "json",
				data: "id="+window.page+"&table=connections&addit_id=view;source_id:"+$(conn.source).attr('id')+";target_id:"+$(conn.target).attr('id'),
				success: function(msg){
					if (msg.status) {
						jsPlumb.detach(conn);
					}
				}
			}).fail(function() {$('#alerts').html("<span style='color:red;'>Не удалось удалить объект</span>").trigger('show-alert'); });
			
	});


	var _getAllConnections = function(id) {
		var cons = jsPlumb.getConnections({target:id }),
		cons2 = jsPlumb.getConnections({source:id });
		Array.prototype.push.apply(cons, cons2);
		return cons;
	};

	var _highlightElems = function(id, hl) {
		var cons = _getAllConnections(id);
		for(var i = 0; i < cons.length; i++) {
				cons[i].setHover(hl);
			if (hl) {
				cons[i].source.addClass("hl");
				cons[i].target.addClass("hl");
			} else {
				cons[i].source.removeClass("hl");
				cons[i].target.removeClass("hl");
			}
		}
		if (hl) {
			$("#" + id).addClass("hl");
			$("#" + id).addClass("hl");
		} else {
			$("#" + id).removeClass("hl");
			$("#" + id).removeClass("hl");
		}
	};
	
	var unhl = function(id, ignoreOpacity) {
	_highlightElems(id, false);
	};
	var hl = function(id) {
	if (hlid) unhl(hlid,true);
	hlid = null;
	_highlightElems(id, true);
	};
	var hlid = null;
	
	$(".connectable, .sourcePoint").hover(function(e) {
	if (e.altKey) {
		hl($(this).attr("id"));
		}
	}, function() {
	hlid = $(this).attr("id");
	unhl(hlid);
	});
	
	
});

if (window.page!='servers') {
	auto_align('all');
	do_count();
	switch (window.page) {
	case 'filial':
		$("#info_active").css('display','inline');
		$("#info_normal").css('display','inline');
		$("#info_strange").css('display','inline');
		$("#info_old").css('display','inline');
		$("#info_total").css('display','inline');
		break
	}
}