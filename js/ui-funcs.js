//Опционный бинд на показ инфо сообщений
$('#alerts').bind("show-alert", function(){
	$('#alerts').fadeIn(30).delay(5000).fadeOut(30);
});

$.autoscroll.init();

//Опционный бинд на показ инфо сообщений
// $('#alerts').bind("make-draggable", function(){
// 	$('#alerts').fadeIn(30).delay(5000).fadeOut(30);
// });

function make_draggable()
{
	if ($(".draggable").length>0) {
		jsPlumb.draggable(jsPlumb.getSelector(".draggable"),
			{
					stop: function(){
					if (window.page=='servers') change_pos(this);
				},
					grid: [ 20,20 ]
			}
		);
	}
}

//Бинд на закрытие инфо сообщения
$('#alerts').bind("click", function(){
	$('#alerts').hide();
});

//убрать-показать дебуг
function hide_expl(){
	if ($('#explanation').css('display')=='none') $('#explanation').show();
	else $('#explanation').hide();
}

function auto_align(what){
	window.line=0;
	window.row=0;
	if (what=="all"){
		$(".alive").show();$(".normal").show();$(".strange").show();$(".old").show();
		align("alive");align("normal");align("strange");align("old");align("rest");
	}
	else {
		switch (what) {
		case "alive":
			align("alive");$(".alive").show();$(".normal").hide();$(".strange").hide();$(".old").hide();
			break
		case "normal":
			align("normal");$(".alive").hide();$(".normal").show();$(".strange").hide();$(".old").hide();
			break
				case "strange":
			align("strange");$(".alive").hide();$(".normal").hide();$(".strange").show();$(".old").hide();
			break
				case "old":
			align("old");$(".alive").hide();$(".normal").hide();$(".strange").hide();$(".old").show();
			break
		}
	}
	
}

function align(what){
	$("."+what).each(function (i) {
        this.style.top=window.line*170+70;
		this.style.left=window.row*170+20;
		if (window.page=='servers') change_pos(this);
		window.row++;
		if ((window.row+1)*170>$('#main').width()){
			window.row=0;
			window.line++;
		}
      });
}

function do_count(){
	var active=count_me("alive");
	$("#info_active_val").text(active);
	var normal=count_me("normal");
	$("#info_normal_val").text(normal);
	var strange=count_me("strange");
	$("#info_strange_val").text(strange);
	var old=count_me("old");
	$("#info_old_val").text(old);
	$("#info_total_val").text(active+normal+strange+old);
}

function count_me(what){
	var counter=0;
	$("."+what).each(function (i) {
		counter++;
      });
	return counter;
}

$("body").on('keyup','#inline_edit',function(event) {
	//console.log(event.which);
	if (event.keyCode==13) $('#inline_accept').click();
	if (event.keyCode==27) $('#inline_delete').click();
});

$("body").on('dblclick','.editable', function() {
	$('#inline_edit_block').remove();
	$('.editable').show();
	el=$(this);
	el.hide();
	myid=el.attr('id');
	elwidth=el.width()+20;
	elid=myid.split('-');
	parent_id=elid[2];
	elid=elid[1];
	table=el.attr('table');
	var m2m="";
	//console.log(table);

	if (el.hasClass('select')) {
		if (table=='services') {
			addit="color";
			par_id=el.attr('m2m')+";services_id;servers_id:"+parent_id;
		}
		else {
			par_id="";
			addit="";
		}
		var element = $.ajax({
			url: "ajax/ajax.getSelect.php",
			async: false,
			dataType: "json",
			data: "table="+table+"&id="+elid+"&addit="+addit+"&parent_id="+par_id
			}).responseText;
	}
	else {
		var element='<input type="text" id="inline_edit" value="'+$(this).text()+'" style="width:'+elwidth+';" />';
	}
	if (el.hasClass('m2m')) {
		table=el.attr('m2m');
		m2m=el.attr('pair');
	}
	
	el.after('<span id="inline_edit_block">'+element+'<span id="inline_controls"><img src="img/accept.png" id="inline_accept" class="cursor" onclick="done_edit(\''+myid+'\','+elid+',true,\''+table+'\',\''+m2m+'\');" /><img src="img/delete.png" id="inline_delete" class="cursor" onclick="done_edit(\''+myid+'\','+elid+',false);" /></span></span>');
	if (el.hasClass('color')) {
			$('#inline_edit').ColorPicker({
				onSubmit: function(hsb, hex, rgb, el) {
					$(el).val(hex);
					$(el).ColorPickerHide();
				},
				onBeforeShow: function () {
					$(this).ColorPickerSetColor(this.value);
				},
				onChange: function (hsb, hex, rgb) {
					$('#inline_edit').val(hex);
				}
			});
			$('#inline_edit').ColorPickerShow();
	}
	$('#inline_edit').focus();
});


function done_edit(myid,elid,update,table,m2m) {
	if (table == null || table=='undefined'){table = window.table_glob;}
	var el=$('#'+myid);
	
	if (update) {
		var val=$('#inline_edit').val();
		if (el.hasClass('select')) var text=$('#inline_edit option:selected').text();
		if (table=="servers2services") var color=$('#inline_edit option:selected').attr('color');
		var name=myid.split('-');
		var values=new Object();
		values[name[0]]=val;
		if (el.hasClass('m2m')) m2m = m2m+";"+name[0]+"-"+name[1];
		//else m2m="";
		 $.ajax({
			type: "POST",
			url: "ajax/bd/ajax.update.php",
			dataType: "json",
			data: "values="+encodeURIComponent(JSON.stringify(values))+"&table="+table+"&id="+elid+"&m2m="+m2m,
			success: function(msg){
				if (msg.status) {
					if (el.hasClass('select')) {
						el.text(text);
						el.attr('id',name[0]+'-'+val+'-'+name[2]);
						$('#servers2services'+name[1]+'-'+name[2]).attr('id','servers2services'+val+'-'+name[2]);
					}
					else el.text(val);
					if (table=="services") $('#services-'+elid).css('background-color',"#"+val);
					if (table=="servers2services") $('#servers2services'+val+'-'+name[2]).css('background-color',"#"+color);
					$('#alerts').html(msg.answer).trigger('show-alert');
				}
			}
		}).fail(function() {$('#alerts').html("<span style='color:red;'>Не удалось обновить объект</span>").trigger('show-alert'); });

	}
	$('#inline_edit_block').remove();
	el.show();
}

function add_item(table,par_id) {
	if (table == null || table=='undefined'){table = window.table_glob;}
	$.ajax({
	type: "POST",
	url: "ajax/bd/ajax.add.php",
	dataType: "json",
	data: "table="+table+"&pair="+par_id,
	success: function(msg){
		if (msg.status) {
			if (par_id == null) $('#insert_point').before(msg.item);
			else  $('#'+table+'_'+par_id).append(msg.item);
			$('#alerts').html(msg.answer).trigger('show-alert');
			make_draggable();
		}
	}
}).fail(function() {$('#alerts').html("<span style='color:red;'>Не удалось добавить объект</span>").trigger('show-alert'); });
}

function delete_item(myid,table,addit_id) {
	if (table == null){table = window.table_glob;}
	if (addit_id == null){addit_id = "";}
	if (confirm('Вы уверены, что хотите удалить элемент с id='+myid+'?')) {
		$.ajax({
			type: "POST",
			url: "ajax/bd/ajax.delete.php",
			dataType: "json",
			data: "id="+myid+"&table="+table+"&addit_id="+addit_id,
			success: function(msg){
				if (msg.status) {
					$('#alerts').html(msg.answer).trigger('show-alert');
					if (addit_id!="") {
						addit_id=addit_id.split(':');
						$('#'+table+myid+'-'+addit_id[1]).remove();
					}
					else $('#'+table+'-'+myid).remove();
				}
			}
		}).fail(function() {$('#alerts').html("<span style='color:red;'>Не удалось удалить объект</span>").trigger('show-alert'); });
	}
}