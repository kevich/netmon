function elem_add(type){
	
}

function change_pos(elem){
	//var offset=$(elem).offset();
	//alert("left: " + offset.left + ", top: " + offset.top + $(elem).attr('id'));
	
	 $.ajax({
		type: "POST",
		url: "ajax/ajax.move.php",
		data: "x="+$(elem).css('left')+"&y="+$(elem).css('top')+"&id="+$(elem).attr('id')
	});
}



$("body").on('click','.movements',function() {
	var url = 'ajax/ajax.movements.php?id='+$(this).attr('move_id');
	// show a spinner or something via css
	var dialog = $('<div style="display:none" class="loading" title="Перемещения - '+$(this).attr('move_id')+'"></div>').appendTo('body');
	// open the dialog
	dialog.dialog({
		// add a close listener to prevent adding multiple divs to the document
		close: function(event, ui) {
			// remove div with all data and events
			dialog.remove();
		},
		modal: true,
		width: '680px'
	});
	// load remote content
	dialog.load(
		url,
		{}, // omit this param object to issue a GET request instead a POST request, otherwise you may provide post parameters within the object
		function (responseText, textStatus, XMLHttpRequest) {
			// remove the loading class
			dialog.removeClass('loading');
		}
	);
	//prevent the browser to follow the link
	return false;
});


$("body").on('click','.control .info',function() {
	var id=$(this).attr('move_id');
	id=id.split('--');
	var url = 'ajax/ajax.editInfo.php?id='+id[1]+'&table='+window.table_glob;
	
	// show a spinner or something via css
	var dialog = $('<div style="display:none" class="loading" title="Информация: '+id[0]+' ('+id[1]+')'+'"></div>').appendTo('body');
	// open the dialog
	dialog.dialog({
		// add a close listener to prevent adding multiple divs to the document
		close: function(event, ui) {
			// remove div with all data and events
			dialog.remove();
		},
		modal: true,
		width: '90%',
		//height: '690',
		position: ['center','top'],
		buttons: {
			"ОК": function() {
				//tinyMCE.triggerSave();
				var values=new Object();
				//values['info']=$('#info_text').html();
				values['info']=tinyMCE.get('info_text').getContent();
				elid=$('#info_text').attr('my_id');
				el=$(this);
				$.ajax({
					type: "POST",
					url: "ajax/bd/ajax.update.php",
					dataType: "json",
					data: "values="+encodeURIComponent(JSON.stringify(values))+"&table="+window.table_glob+"&id="+elid+"&m2m=",
					success: function(msg){
						if (msg.status) {
							el.dialog("close");
							$('#alerts').html("Информация обновлена").trigger('show-alert');
						}
						else {
							el.dialog("close");
							$('#alerts').html("<span style='color:red;'>Не удалось обновить объект</span>").trigger('show-alert');
						}
					}
				}).fail(function() {$('#alerts').html("<span style='color:red;'>Не удалось обновить объект</span>").trigger('show-alert');$(this).dialog("close");});
			},
			"Отмена": function() {
				$(this).dialog("close");
			}
		}
	});
	// load remote content
	dialog.load(
		url,
		{}, // omit this param object to issue a GET request instead a POST request, otherwise you may provide post parameters within the object
		function (responseText, textStatus, XMLHttpRequest) {
			// remove the loading class
			dialog.removeClass('loading');
		}
	);
	//prevent the browser to follow the link
	return false;
});

function get_secret(el) {
	if ($(el).hasClass('exposed')) {
		$('#pass_'+$(el).text()).remove();
		$(el).removeClass('exposed');
	}
	else {
		$.ajax({
			type: "POST",
			url: "ajax/ajax.showSecret.php",
			data: "name="+$(el).text(),
			success: function(msg){
							$(el).after('<span id="pass_'+$(el).text()+'">'+msg+'</span>');
							$(el).addClass('exposed');
						}
		});
	}

}