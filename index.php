<?php

foreach (glob("./classes/*.class.php") as $filename) include_once $filename;

include_once 'config.php';
include_once 'functions/wrappers.php';


if (array_key_exists('view', $_REQUEST)) $view=$_REQUEST['view'];
if (array_key_exists('id', $_REQUEST)) $ID=$_REQUEST['id'];


$GLOBALS['DBG']->debug=false;
$GLOBALS['DBG']->out="";

$GLOBALS['NETMON_SQL']=new MySQL;
$GLOBALS['NETMON_SQL']->connect();

$title="";

if ($view=="filial" && $ID!="all"){
	$res=$GLOBALS['NETMON_SQL']->exec_SELECTquery("name",'td_filialsName',"sub=$ID");
	$row=mysqli_fetch_assoc($res);
	$title="<b>Филиал:</b> ".$row['name'];
}

?>

<html>
	<head>
		<title>NetMon - <?php echo strip_tags($title); ?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<link type="text/css" href="css/smoothness/jquery-ui-1.8.20.custom.css" rel="Stylesheet" />
		<link rel="stylesheet" href="css/style.css" />
		<link rel="stylesheet" href="css/colorpicker/colorpicker.css" />
		<link rel="shortcut icon" href="favicon.ico" />
		<script type='text/javascript' src='js/jquery-1.7.2.min.js'></script>
		<script type='text/javascript' src='js/jquery-ui-1.8.20.custom.min.js'></script>
		<script type='text/javascript' src='js/jquery.jsPlumb-1.3.9-all.js'></script>
		<script type='text/javascript' src='js/colorpicker.js'></script>
	</head>
	<body>
		<?php echo "<script type=\"text/javascript\">window.page='$view';</script>"; ?>
		
		<div class="menu">
		<ul>
			<li><a href="#">Раздел</a>
				<ul>
					<li><a href="?view=servers">Сервера</a></li>
					<li>
						<a href="?view=lans">Филиалы</a>
						<!--<ul>
						<?php
// 							$res=$GLOBALS['NETMON_SQL']->exec_SELECTquery('name','td_filialsName');
// 							while ($row=mysqli_fetch_assoc($res)) {
// 								echo "<li><a href=\"#\">".$row['name']."</a></li>";
// 							}
						?>
						</ul>-->
					</li>
					<li><a href="?view=services">Сервисы</a></li>
				</ul>
			</li>
			<li><a href="#" onclick="auto_align('all');return false;">auto align</a></li>
			<?php if ($GLOBALS['DBG']->debug) echo '<li><a href="#" onclick="hide_expl();return false;">debug show/hide</a></li>';?><li>
			<li id="menu_point" style="display:none;"></li>
		</ul>
    </div>

    <div id="info"><?php echo $title; ?>
		<div id="info_total"><b><a href="#" onclick="auto_align('all');">Всего:</a> </b><span id="info_total_val">0</span></div>
		<div id="info_active"><a href="#" onclick="auto_align('alive');">Активных:</a> <span id="info_active_val">0</span></div>
		<div id="info_normal"><a href="#" onclick="auto_align('normal');">Выключенных:</a> <span id="info_normal_val">0</span></div>
		<div id="info_strange"><a href="#" onclick="auto_align('strange');">Странных:</a> <span id="info_strange_val">0</span></div>
		<div id="info_old"><a href="#" onclick="auto_align('old');">Старых:</a> <span id="info_old_val">0</span></div>
    </div>
		<div id="main">
			<?php
			$where="";
			if (isset($ID)) {
				if ($ID=="all") $where="";
				else $where="subnetwork=$ID";
			}
			foreach($classes[$view]::getItems($where) as $item) {
						echo $item->draw_item();
					}
			$classes[$view]::GetJsModule();
			echo "<span id=\"insert_point\"></span>";
			?>
		</div>
		<div id="explanation" style="display:none;">
				<?php if ($GLOBALS['DBG']->debug) echo $GLOBALS['DBG']->out; ?>
		</div>
		<div id="alerts" style="display:none;"></div>
		<script type="text/javascript" src="js/autoscroll.js"></script>
		<script type="text/javascript" src="js/ui-funcs.js"></script>
		<script type="text/javascript" src="js/main.js"></script>
		<script type="text/javascript" src="js/ajax.js"></script>
		<script type="text/javascript" src="js/tiny_mce/tiny_mce.js"></script>
		
		
		<script type="text/javascript" src="js/prepare-connections.php?view=<?=$view;?>"></script>
		
		
		
		
 </body>
</html>
<?php $GLOBALS['NETMON_SQL']->close(); ?>